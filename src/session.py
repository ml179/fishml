import dill
from os import path


def dump(name):
    f = f'/home/jupyter/work/resources/saves/{name}.pkl'
    dill.dump_session(f)
    print(f'session dumped to {f}')


def load(name):
    f = f'/home/jupyter/work/resources/saves/{name}.pkl'
    if path.exists(f):
      dill.load_session(f)
      print(f'session loaded from {f}')
    else:
      print(f"file does not exist {f}")