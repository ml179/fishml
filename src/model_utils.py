import tensorflow as tf
physical_devices = tf.config.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(physical_devices[0], True)
import logging
tf.get_logger().setLevel(logging.ERROR)
tf.debugging.experimental.disable_dump_debug_info()
from utils import *
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import accuracy_score

def load_dataset(it, cfg, fg_train, fg_test):
    output_signature = (tf.TensorSpec(shape = (None, None, None, 3), dtype = tf.float32),
                    tf.TensorSpec(shape = (), dtype = tf.int16))
    train_ds = tf.data.Dataset.from_generator(fg_train, output_signature = output_signature)
    train_ds = train_ds.batch(cfg['batches'])

    test_ds = tf.data.Dataset.from_generator(fg_test, output_signature = output_signature)
    test_ds = test_ds.batch(cfg['batches'])
    return train_ds, test_ds


def train_model(it, cfg, model, train_ds, test_ds):
    if cfg['loss'] == 'SparseCategoricalCrossentropy':
        loss_obj = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

    if cfg['optimizer'] == 'Adam':
        optimizer = tf.keras.optimizers.Adam(learning_rate = cfg['learning_rate'])
    elif cfg['optimizer'] == 'AdamW':
        optimizer = tf.keras.optimizers.AdamW(learning_rate = cfg['learning_rate'])
    metrics = ["accuracy"]

    model.compile(loss=loss_obj, optimizer=optimizer, metrics=metrics)
    
    history = model.fit(train_ds,
                    validation_data=test_ds,
                    epochs=cfg['epochs'],
                    validation_freq=1) 
    
    model.save(f'{iter_path(it)}/model')
    iter_save(it, history.history, 'train_history')
    
    return model


def test_model(it, cfg, model, ds, fg):
    predicted = model.predict(ds)
    actual = fg.get_actual_labels()

    actual = tf.stack(actual, axis=0)
    predicted = tf.concat(predicted, axis=0)
    predicted = tf.argmax(predicted, axis=1)

    accuracy = accuracy_score(actual, predicted)
    precision, recall, fscore, count = precision_recall_fscore_support(actual, predicted)

    print(f'accuracy: {accuracy}')
    class_metrics = {}
    for l, i in fg.get_labels().items():
        print(f'{l} -- precision: {precision[i]}, recall: {recall[i]}, fscore: {fscore[i]}, count: {count[i]}')
        class_metrics[l] = {
            'precision': precision[i],
            'recall': recall[i],
            'fscore': fscore[i],
            'count': count[i]
        }

    metrics = {
        'metrics': {
            'accuracy': accuracy
        },
        'class_metrics': class_metrics
    }

    iter_save(it, metrics, f'metrics_{fg.select}')
    iter_save(it, actual, f'actual_{fg.select}')
    iter_save(it, predicted, f'predicted_{fg.select}')

