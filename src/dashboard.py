import tensorflow as tf
tf.config.set_visible_devices([], 'GPU')
from dash import Dash, html, dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output
import plotly.express as px
import numpy as np
import os
import io
import plotly.graph_objs as go
import numpy as np
from sklearn.metrics import confusion_matrix

from src.utils import *


SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "16rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
    "width": 200,
    "border-right": "solid 1px",
    "border-color": "#0d6efd"
}


CONTENT_STYLE = {
    "margin-left": "12rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}

CFG_STYLE = {
    "border-right": "solid 1px",
    "border-color": "#0d6efd"
}

STATS_STYLE = {

}
    
TRAIN_METRICS_STYLE = {
    "border-right": "solid 1px",
    "border-color": "#0d6efd"
}
    
TEST_METRICS_STYLE = {
    
}

def print_stat(f, k, v, prefix):
    if v['count'] == 0:
        print(f'{prefix}{k} ({v["count"]}/{v["total_count"]}) - Не найден!', file=f)
    elif v['count'] < v['total_count']:
        print(f'{prefix}{k} ({v["count"]}/{v["total_count"]}) - Не хватает!', file=f)
    else:
        print(f'{prefix}{k} ({v["count"]}/{v["total_count"]}) - Полный', file=f)


class Dashboard:
    def __init__(self):
        self.iterations = {}

    def render_previews(self, preview_frames):
        previews = []
        for f, l in preview_frames:
            previews.append(dcc.Graph(
                figure=px.imshow(np.array(f), facet_col=0, facet_col_wrap=2, title=l)
            ))
        previews = html.Div(previews, style={'overflow': 'scroll', 'height':'80vh', 'width': 500})
        return previews

    def render_config(self, cfg):
        f = io.StringIO()

        for k, v in cfg.items():
            print(f'{k}: {v}', file=f)

        contents = f.getvalue()
        f.close()
        return html.Div([
            html.H2(f'Параметры обучения', className="display-7"),
            html.Plaintext(contents, style=CFG_STYLE)
        ])

    def render_stats(self, stats):
        f = io.StringIO()

        for select, ops in stats.items():
            print_stat(f, select, ops, '')
            for mt, mops in stats[select]['subtags'].items():
                print_stat(f, mt, mops, '\t')
                for k, v in mops['tag_stats'].items():
                    print_stat(f, k, v, '\t\t')

        contents = f.getvalue()
        f.close()
        return html.Div([
            html.H2(f'Параметры датасета', className="display-7"),
            html.Plaintext(contents, style=STATS_STYLE)
        ])

    def render_metrics(self, metrics, select):
        f = io.StringIO()
        print('Общее', file=f)
        for m, v in metrics['metrics'].items():
            print(f'\t{m}: {v}', file=f)

        for t, ms in metrics['class_metrics'].items():
            print(t, file=f)
            for m, v in ms.items():
                print(f'\t{m}: {v}', file=f)

        contents = f.getvalue()
        f.close()
        style = {}
        if select == 'Тренировочные':
            style = TRAIN_METRICS_STYLE
        return html.Div([
            html.H2(f'{select} метрики', className="display-7"),
            html.Plaintext(contents, style=style)
        ])

    def render_confusion_matrics(self, actual, predicted, labels, title):
        cm = confusion_matrix(actual, predicted)
        heatmap = go.Heatmap(z=cm, x=list(labels), y=list(labels), colorscale='Blues')
                
        layout = go.Layout(title='Confusion Matrix')
        fig = go.Figure(data=[heatmap], layout=layout)
        fig.update_layout(xaxis_title="Predicted", yaxis_title="Actual")

        return html.Div([
            html.H2(title, className="display-7"),
            dcc.Graph(figure=fig)
        ])
    
    def render_metric(self, history, metric, title):
        if history == "":
            return html.Div()

        fig = go.Figure()
        fig.add_trace(
           go.Scatter(x=np.arange(len(history[metric])), y=history[metric], mode='lines+markers', name='Train'))
        fig.add_trace(
           go.Scatter(x=np.arange(len(history[f'val_{metric}'])), y=history[f'val_{metric}'], mode='lines+markers', name='Validation'))
       
        fig.update_layout(xaxis_title="Epoch", yaxis_title="Loss")
    
        return html.Div([
           html.H2(title, className="display-7"),
           dcc.Graph(figure=fig)
        ])
       
    def render_iteration(self, it):
        cfg = iter_read(it, 'cfg')
        comment = iter_read(it, 'comment')
        labels = iter_read(it, 'labels')
        preview_frames = iter_read(it, 'preview_frames')
        train_history = iter_read(it, 'train_history')
        stats = iter_read(it, 'stats')
        train_metrics = iter_read(it, 'metrics_обучение')
        test_metrics = iter_read(it, 'metrics_тест')
        train_actual = iter_read(it, 'actual_обучение')
        test_actual = iter_read(it, 'actual_тест')
        train_predicted = iter_read(it, 'predicted_обучение')
        test_ptedicted = iter_read(it, 'predicted_тест')

        view_previews = self.render_previews(preview_frames)
        view_config = self.render_config(cfg)
        view_stats = self.render_stats(stats)
        if train_metrics != "":
            view_train_metrics = self.render_metrics(train_metrics, 'Тренировочные')
            view_test_metrics = self.render_metrics(test_metrics, 'Тестовые')
            view_train_matrix = self.render_confusion_matrics(train_actual, train_predicted, labels, 'Обучение')
            view_test_matrix = self.render_confusion_matrics(test_actual, test_ptedicted, labels, 'Тест')
            view_loss = self.render_metric(train_history, 'loss', 'Loss')
            view_accuracy = self.render_metric(train_history, 'accuracy', 'Accuracy')
        else:
            view_train_metrics = html.Div()
            view_test_metrics = html.Div()
            view_train_matrix = html.Div()
            view_test_matrix = html.Div()
            view_loss = html.Div()
            view_accuracy = html.Div()

        view_comment = html.Div([html.H2('Предпросмотр Датасета')])
        if len(comment) > 0:
            view_comment = html.Div([
                html.H2('Комментарий', className="display-7"),
                html.Plaintext(comment),
                html.H2('Предпросмотр Датасета')
            ])

        return html.Div([
            dbc.Row([
                dbc.Col([
                    view_comment,
                    view_previews
                ], md=4),
                dbc.Col([
                    dbc.Row([
                        dbc.Col(view_config, md=5),
                        dbc.Col(view_stats, md=5)
                    ]),
                    html.Hr(),
                    dbc.Row([
                        dbc.Col(view_train_metrics, md=5),
                        dbc.Col(view_test_metrics, md=5)
                    ]),
                    html.Hr(),
                    dbc.Row([
                        dbc.Col(view_train_matrix, md=5),
                        dbc.Col(view_test_matrix, md=5)
                    ]),
                    html.Hr(),
                    dbc.Row([
                        view_loss,
                        view_accuracy
                    ])
                ], style={'overflow': 'scroll', 'height':'95vh'}),
            ])
        ])

    def render_layout(self):
        models = []
        num_models = len(os.listdir(IT_PATH))

        for model in os.listdir(IT_PATH):
            model_tabs = []
            def filter(x):
                r = ''
                for s in x:
                    if s.isdigit():
                        r += s
                    else:
                        break
                return int(r)
            
            for it in sorted(os.listdir(f'{IT_PATH}/{model}'), key=filter):
                model_tabs.append(dbc.NavLink(f'{it}', href=f"/{model}/{it}", active="exact"))

            models.append(html.Div([
                html.H2(f'{model}', className="display-7"),
                html.Div([
                    dbc.Nav(model_tabs, vertical=True, pills=True),
                ], style={
                            'overflow': 'scroll',
                            'height':f'{(100-num_models*10)//num_models}vh'
                })
            ]))

        sidebar = html.Div(models, style=SIDEBAR_STYLE)
        content = html.Div(id="page-content", style=CONTENT_STYLE)
                
        return html.Div([dcc.Location(id="url"), sidebar, content])

    def run(self):
        app = Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])
        app.layout = self.render_layout

        @app.callback(Output("page-content", "children"), [Input("url", "pathname")])
        def render_page_content(it):
            return self.render_iteration(it)

        app.run_server(debug=True, host='0.0.0.0', port=8080)