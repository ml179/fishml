import tensorflow as tf

class Conv2Plus1D(tf.keras.layers.Layer):
  def __init__(self, filters, kernel_size, padding):
    """
      A sequence of convolutional layers that first apply the convolution operation over the
      spatial dimensions, and then the temporal dimension. 
    """
    super().__init__()
    self.seq = tf.keras.Sequential([  
        # Spatial decomposition
        tf.keras.layers.Conv3D(filters=filters,
                      kernel_size=(1, kernel_size[1], kernel_size[2]),
                      padding=padding),
        # Temporal decomposition
        tf.keras.layers.Conv3D(filters=filters, 
                      kernel_size=(kernel_size[0], 1, 1),
                      padding=padding)
        ])

  def call(self, x):
    return self.seq(x)
  

class ResidualMain(tf.keras.layers.Layer):
  """
    Residual block of the model with convolution, layer normalization, and the
    activation function, ReLU.
  """
  def __init__(self, filters, kernel_size):
    super().__init__()
    self.seq = tf.keras.Sequential([
        Conv2Plus1D(filters=filters,
                    kernel_size=kernel_size,
                    padding='same'),
        tf.keras.layers.LayerNormalization(),
        tf.keras.layers.ReLU(),
        Conv2Plus1D(filters=filters, 
                    kernel_size=kernel_size,
                    padding='same'),
        tf.keras.layers.LayerNormalization()
    ])

  def call(self, x):
    return self.seq(x)
  

class Project(tf.keras.layers.Layer):
  """
    Project certain dimensions of the tensor as the data is passed through different 
    sized filters and downsampled. 
  """
  def __init__(self, units):
    super().__init__()
    self.seq = tf.keras.Sequential([
        tf.keras.layers.Dense(units),
        tf.keras.layers.LayerNormalization()
    ])

  def call(self, x):
    return self.seq(x)
  

def add_residual_block(input, filters, kernel_size):
  """
    Add residual blocks to the model. If the last dimensions of the input data
    and filter size does not match, project it such that last dimension matches.
  """
 
  out = ResidualMain(filters, 
                     kernel_size)(input)

  res = input
  # Using the Keras functional APIs, project the last dimension of the tensor to
  # match the new filter size
  if out.shape[-1] != input.shape[-1]:
    res = Project(out.shape[-1])(res)

  return tf.keras.layers.add([res, out])


class ResizeVideo(tf.keras.layers.Layer):
  def __init__(self, ph, pw, h, w, num_frames):
    super().__init__()

    self.seq = tf.keras.Sequential([
        tf.keras.layers.Reshape((ph, pw, -1)),
        # Rearrange('b t h w c -> (b t) h w c'),
        tf.keras.layers.Resizing(h, w),
        #Rearrange('(b t) h w c -> b t h w c', t = num_frames)
        tf.keras.layers.Reshape((num_frames, h, w, -1))
    ])

  def call(self, x):
    return self.seq(x)
  

def load_conv3d(cfg, labels):
    HEIGHT = cfg['height']
    WIDTH = cfg['width']
    NUM_FRAMES = cfg['frames']

    input_shape = (None, NUM_FRAMES, HEIGHT, WIDTH, 3)
    input = tf.keras.layers.Input(shape=(input_shape[1:]))
    x = input

    x = Conv2Plus1D(filters=16, kernel_size=(3, 7, 7), padding='same')(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.ReLU()(x)

    x = ResizeVideo(HEIGHT, WIDTH, HEIGHT // 2, WIDTH // 2, NUM_FRAMES)(x)

    # Block 1
    x = add_residual_block(x, 16, (3, 3, 3))
    x = ResizeVideo(HEIGHT // 2, WIDTH // 2, HEIGHT // 4, WIDTH // 4, NUM_FRAMES)(x)

    # Block 2
    x = add_residual_block(x, 32, (3, 3, 3))
    x = ResizeVideo(HEIGHT // 4, WIDTH // 4, HEIGHT // 8, WIDTH // 8, NUM_FRAMES)(x)

    # Block 3
    x = add_residual_block(x, 64, (3, 3, 3))
    x = ResizeVideo(HEIGHT // 8, WIDTH // 8, HEIGHT // 16, WIDTH // 16, NUM_FRAMES)(x)

    # Block 4
    x = add_residual_block(x, 128, (3, 3, 3))
    x = ResizeVideo(HEIGHT // 16, WIDTH // 16, HEIGHT // 32, WIDTH // 32, NUM_FRAMES)(x)
    
     # Block 5
    x = add_residual_block(x, 256, (3, 3, 3))

    x = tf.keras.layers.GlobalAveragePooling3D()(x)
    x = tf.keras.layers.Flatten()(x)
    x = tf.keras.layers.Dense(10)(x)

    model = tf.keras.Model(input, x)
    return model
