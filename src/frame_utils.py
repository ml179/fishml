import yaml
import dill
import os
import random
import cv2
import numpy as np
from skimage import exposure
from skimage import transform
from skimage import util 
from utils import *


def prepare_format(frames):
    res = []
    for frame in frames:
        frame = frame.astype(np.float32)
        frame /= 255
        res.append(frame)
    return np.array(res)


def augment_video(frames):
    def apply_transforms(transforms, image):
        for t in transforms:
            enabled = t[0]
            func = t[1]
            args = t[2:]
            if enabled:
                image = func(image, *args)
        return image
    
    def prep(img):
        if img.dtype == np.float32:
            img *= 255
            img = img.astype(np.uint8)
        return img

    def adjust_gamma(image, gamma=1.0):
        invGamma = 1.0 / gamma
        table = np.array([((i / 255.0) ** invGamma) * 255
            for i in np.arange(0, 256)]).astype("uint8")

        return cv2.LUT(image, table)

    def equlize(img):
        # img[:, :, 0] = exposure.equalize_hist(img[:, :, 0])
        # img[:, :, 1] = exposure.equalize_hist(img[:, :, 1])
        # img[:, :, 2] = exposure.equalize_hist(img[:, :, 2])

        img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)

        # equalize the histogram of the Y channel
        img_yuv[:,:,0] = cv2.equalizeHist(img_yuv[:,:,0])

        # convert the YUV image back to RGB format
        img_output = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)
        return img_output
    
    def rotate_image(image, angle):
        image_center = tuple(np.array(image.shape[1::-1]) / 2)
        rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
        result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
        return result

    simple_transforms = [
        (True, prep),
        (True, equlize),
        (random.randint(0, 1), adjust_gamma, random.uniform(0.3, 2.5)),
    ]
    
    spatial_transforms = [
        (True, rotate_image, random.randint(0, 360)),
        (random.randint(0, 1), np.flipud),
        (random.randint(0, 1), np.fliplr)
    ]
        
    return [apply_transforms(simple_transforms + spatial_transforms, i) for i in frames]


class FrameGenerator:
    def __init__(self, cfg, select):
        self.select = select
        self.dataset_fd = f'/home/jupyter/mnt/datasets/FISH/{cfg["dataset"]}'
        self.n_frames = cfg['frames']
        self.frame_step = cfg['frame_step']
        self.width = cfg['width']
        self.height = cfg['height']
        self.video_process_options = []
        
        if select == 'обучение':
            self.selects = cfg['train_selects']
        elif select == 'тест':
            self.selects = cfg['test_selects']
        
        with open(f'{self.dataset_fd}/stats.yaml', 'r') as fp:
            self.stats = yaml.load(fp, Loader=yaml.UnsafeLoader)

        with open(f'{self.dataset_fd}/selected_videos.pkl', 'rb') as fp:
            self.selected_videos = dill.load(fp)

        self.label_names = list(self.stats[select]['subtags'].keys())
        self.label_indices = {n: i for i, n in enumerate(self.label_names)}
    
    def prepare(self):
        random.shuffle(self.video_process_options)

    def assign_label(self, ops):
        for k, v in self.label_indices.items():
            if k in ops['tags']:
                return v

    def get_labels(self):
        return self.label_indices

    def video_stat(self, ops):
        for mt, mstats in self.stats[self.select]['subtags'].items():
            if mt in ops['tags']:
                return mstats
    
    def select_videos(self):
        return [(v, ops) for v, ops in self.selected_videos.items() if ops['выборка'] == self.select ]
    
    def init_default_videos(self):
        self.video_process_options = [(v, []) for v, _ in self.select_videos() ]
    
    def apply_transform(self, transform):
        for v, ops in self.video_process_options:
            ops.append(transform)

    def augment_videos(self, n_repeats, augment_func=augment_video):
        for vid, ops in self.select_videos():
            stats = self.video_stat(ops)
            stats['n_augments'] = stats['total_count'] * (n_repeats + 1) - stats['count']
    
        while True:
            added = False
            for vid, ops in self.video_process_options:
                stats = self.video_stat(self.selected_videos[vid])
                if stats['n_augments'] > 0:
                    new_ops = list(ops)
                    new_ops.append(augment_func)
                    
                    self.video_process_options.append((vid, new_ops))
                    stats['n_augments'] -= 1
                    added = True
            
            if not added:
                break
                
        for _, stats in self.stats[self.select]['subtags'].items():
            if 'n_augments' in stats:
                del stats['n_augments']
            
    def extract_frames(self, name):
        fd = f'{self.dataset_fd}/frames/{name}'
        images = list(os.listdir(fd))
        result = []
        
        select_range = self.n_frames * self.frame_step
        if len(images) < select_range:
            return None
        select_begin = random.randint(0, len(images) - select_range)
        
        for i in range(select_begin, select_begin + select_range, self.frame_step):
            path = f'{fd}/{str(i + 1).zfill(4)}.jpg'
            img = cv2.imread(path)
            img = cv2.resize(img, (self.width, self.height))
            result.append(img)
        
        result = np.array(result)[..., [2, 1, 0]]
        return result
    
    def get_actual_labels(self):
        return self.actual_labels

    def __call__(self):
        print(self.selects)
        self.actual_labels = []
        random.shuffle(self.video_process_options)
        cnt = 0
        while True:
            for vid, ops in self.video_process_options:
                selected_ops = self.selected_videos[vid]
                if selected_ops['выборка'] == self.select:
                    video_frames = self.extract_frames(selected_ops['name'])
                    if video_frames is None:
                        continue
                    
                    for op in ops:
                        video_frames = op(video_frames)
                    
                    label = self.assign_label(selected_ops)
                    self.actual_labels.append(label)

                    video_frames = prepare_format(video_frames)
                    yield video_frames, label

                cnt = cnt + 1
                if cnt == self.selects:
                    break
            if cnt == self.selects:
                    break
        self.actual_labels = np.array(self.actual_labels)


def preview_dataset(it, cfg):
    fg = FrameGenerator(cfg, 'обучение')
    fg.init_default_videos()
    gen = fg()

    labels = fg.get_labels().keys()
    
    preview_frames = []
    for _ in range(10):
        frames, label = next(gen)
        if cfg['frame_augments'] >= 1:
            a = np.concatenate([frames[3:4], augment_video(frames[3:4])])
        else:
            a = frames[3:4]
        preview_frames.append((a, fg.label_names[label]))

    iter_save(it, preview_frames, 'preview_frames')
    iter_save(it, labels, 'labels')


def make_generators(it, cfg):
    with open(f'/home/jupyter/mnt/datasets/FISH/{cfg["dataset"]}/stats.yaml', 'r') as fp:
        stats = yaml.load(fp, Loader=yaml.UnsafeLoader)
        iter_save(it, stats, 'stats')

    with open(f'/home/jupyter/mnt/datasets/FISH/{cfg["dataset"]}/selected_videos.pkl', 'rb') as fp:
        selected_videos = dill.load(fp)
        iter_save(it, selected_videos, 'selected_videos')

    fg_train = FrameGenerator(cfg, 'обучение')
    fg_train.init_default_videos()

    if cfg['frame_augments'] > 0:
        fg_train.augment_videos(cfg['frame_augments'])
    fg_train.prepare()

    fg_test = FrameGenerator(cfg, 'тест')
    fg_test.init_default_videos()
    fg_test.prepare()
    return fg_train, fg_test