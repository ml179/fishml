import dill
import os
import shutil

IT_PATH = '/home/jupyter/work/resources/iterations'


def iter_path(iteration):
    return f'{IT_PATH}/{iteration}'


def iter_save(it, obj, name):
    with open(f'{iter_path(it)}/{name}', 'wb') as fp:
        dill.dump(obj, fp)


def iter_read(it, name):
    if os.path.exists(f'{iter_path(it)}/{name}'):
        with open(f'{iter_path(it)}/{name}', 'rb') as fp:
            return dill.load(fp)
    else:
        return ''


def iter_begin(it, cfg, comment):
    if os.path.isdir(iter_path(it)):
        shutil.rmtree(iter_path(it))
    os.makedirs(iter_path(it), exist_ok=True)
    iter_save(it, cfg, 'cfg')
    iter_save(it, comment, 'comment')