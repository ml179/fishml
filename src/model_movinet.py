import os
import wget
import tarfile
import tensorflow as tf
from official.projects.movinet.modeling import movinet
from official.projects.movinet.modeling import movinet_model

def load_movinet(cfg, labels):
    tf.keras.backend.clear_session()
    model_id = cfg['movinet_model']

    backbone = movinet.Movinet(model_id=model_id)
    backbone.trainable = False
    # Set num_classes=600 to load the pre-trained weights from the original model
    model = movinet_model.MovinetClassifier(backbone=backbone, num_classes=600)
    model.build([None, None, None, None, 3])
    url = f'https://storage.googleapis.com/tf_model_garden/vision/movinet/movinet_{model_id}_base.tar.gz'
    
    m_path = '/tmp/'
    os.makedirs(m_path, exist_ok=True)
    filename = wget.download(url, m_path)
    my_tar = tarfile.open(filename)
    my_tar.extractall(m_path) # specify which folder to extract to
    my_tar.close()
    checkpoint_dir = f'{m_path}/movinet_{model_id}_base'
    checkpoint_path = tf.train.latest_checkpoint(checkpoint_dir)
    checkpoint = tf.train.Checkpoint(model=model)
    status = checkpoint.restore(checkpoint_path)
    status.assert_existing_objects_matched()

    model = movinet_model.MovinetClassifier(
        backbone=backbone,
        num_classes=len(labels),
        dropout_rate=cfg['dropout_rate']
    )
    model.build([cfg['batches'], cfg['frames'], cfg['height'], cfg['width'], 3])

    return model
